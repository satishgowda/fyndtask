import axios from 'axios';

const api = axios.create({
  'Content-Type': 'application/json'
});

const getImagesApi = () => api.get('http://demo4126999.mockable.io/images');

export  { getImagesApi };