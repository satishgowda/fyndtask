import Vue from 'vue'
import Vuex from 'vuex'
import { getImagesApi } from './api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    imagesList: [],
    selectedImage: null,
  },
  mutations: {
    saveImagesList(state, images) {
      state.imagesList = images;
    },
    imageToEdit(state, imageObj) {
      state.selectedImage = imageObj;
    },
    removeImage(state, imgObj) {
      state.imagesList = state.imagesList.filter(img => JSON.stringify(img) !== JSON.stringify(imgObj));
    },
    editImage(state, url) {
      state.selectedImage = { ...state.selectedImage, image_url: url}
      const index = state.imagesList.findIndex((imgObj) => imgObj.name === state.selectedImage.name);
      state.imagesList[index] = state.selectedImage;
    }
  },
  actions: {
    getImages ({commit}) {
      return new Promise((resolve, reject) => {
        getImagesApi()
        .then((response) => {
          commit('saveImagesList', response.data);
          resolve();
        })
        .catch((error) => {
          reject(error);
        })
      })
    },
    imageToEdit({ commit }, imgObj) {
      commit('imageToEdit', imgObj);
    },
    removeImage({ commit }, imgObj) {
      commit('removeImage', imgObj);
    },
    editImage({commit}, url) {
      commit('editImage', url);
    }
  }
})
